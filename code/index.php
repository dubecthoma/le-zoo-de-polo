<code><!DOCTYPE html>
    <html>
      <head>
        <script src="function.js"></script>
        <meta charset="utf-8">
        <link href="style.css" rel="stylesheet">
		
		<?php
   
date_default_timezone_set('Europe/Paris');
$date=date('h'); 

if ($date >= 6 && $date < 12) {
echo '<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Burchell%27s_Zebra_%28Etosha%29.jpg/1200px-Burchell%27s_Zebra_%28Etosha%29.jpg">';
}
elseif ($date >= 12 && $date < 18 ) {
echo '<img src="https://cdn-images.zoobeauval.com/P0aysEMKZ6debyVEgw9cSZiLrMc=/730x730/https%3A%2F%2Fs3.eu-west-3.amazonaws.com%2Fimages.zoobeauval.com%2F2020%2F06%2Fsticker-2-5ee34d6b02c7c.jpg">';
}
elseif ($date >= 18 || $date < 6) {
echo '<img src="https://upload.wikimedia.org/wikipedia/commons/0/0f/Grosser_Panda.JPG" style="width:500px;height:500px;">';
}
?>
        <title>Le Zoo de Polo</title>
      </head>
      <body>
        <div class = "colors">
        <ul class="menu">
          <li><a href="#">Accueil</a></li>
          <li><a href="http://zoodepolo.alwaysdata.net/zoo/">Access to the website</a></li>
          <li><a href="http://zoodepolo.alwaysdata.net/formTicket.html">Ticket</a></li>
          <li><a href="http://zoodepolo.alwaysdata.net/legislation.html">Legislation</a></li>
          <li><a href="http://zoodepolo.alwaysdata.net/authentification.html">connection</a></li>
          <li><a href="http://zoodepolo.alwaysdata.net/inscription.php">register</a></li>
        </ul>
        <div class="accueil">
          <h1> Le Zoo de Polo</h1>

          <h2> Sector 1 : Orangutans </h2>
          <div class="sector 1">
             <div class="forme">
               <div class="zoom">
                 <img src="https://zoo-palmyre.fr/sites/default/files/styles/sans_cadre/public/couverture_animaux/_mg_9075.jpg?itok=dyg3bHB5" alt="Les orangs-outangs" style="width:500px;height:500px;">
                </div>
               <br> Come and discover our 180 orangutans in a forest area of ​​over 80km². They are diurnal great apes, with sparse rusty to dark orange fur, which, like other hominoids, do not have a tail. They are endemic to the tropical forests of the islands of Sumatra and Borneo, territories shared between Indonesia and Malaysia.
              </div>
           </div>

           <h2> Sector 2 : Black Panther</h2>

            <div class="sector 2">
              <div class="forme">
                <div class="zoom">
                  <img src="https://geo.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Fgeo.2F2020.2F11.2F05.2Fa162e181-81ef-42c7-8ec9-db7003ce14ed.2Ejpeg/1200x630/cr/wqkgd2lraW1lZGlhIGNvbW1vbnMgLyBHRU8%3D/la-panthere-noire-darmentieres-derobee-il-y-a-un-an-finalement-retrouvee-aux-pays-bas.jpg" alt="Les orangs-outangs" style="width:500px;height:500px;">
                </div>
               <br> Come and discover our 150 black panthers in a plain area of ​​over 60km². The black panther is a carnivorous mammal (which eats raw meat) from Africa and Asia. It is in fact a leopard with a dark coat (melanism). She is one of the felines.
              </div>
            </div>

            <h2> Sector 3 : Koala</h2>
            <div class="sector 3">
              <div class="forme">
                <div class="zoom">
                  <img src="https://services.meteored.com/img/article/koala-declarado-en-peligro-de-extincion-en-australia-deforestacion-caceria-virus-lisita-roja-ambientalistas-crisis-ambiental-1644712287296_1024.jpeg" alt="Les orangs-outangs" style="width:500px;height:500px;">
                </div>
                <br> Come and discover our 50 koalas in a forest area of ​​over 100km². The koala has a body length of 60–85 cm (24–33 in) and weighs 4–15 kg (9–33 lb). Fur colour ranges from silver grey to chocolate brown.
              </div>
            </div>

            <h2> Sector 4 : Brown Bear</h2>
            <div class ="sector 4">
              <div class="forme">
                <div class ="zoom">
                 <img src="https://zoo-amneville.com/wp-content/uploads/2020/05/ours-brun-repos_adrien-farese_22.04.21.jpg" alt="Les orangs-outangs" style="width:500px;height:500px;">
                </div>
                <br> Come and discover our 5 brown bears in the rocks, in an area of 10km².The brown bear can live thirty years in the wild and up to forty years in captivity. The brown bear has fur in shades of blonde, brown, black, or a combination of these colors. Brown bears have a large hump of muscle above their shoulders which gives strength to the forelimbs for digging. Their head is large and round with a concave facial profile. Standing, the bear reaches a height of 1.5-3.5 meters. Despite their size, they can run at speeds of up to 55 km/h.
              </div>
            </div>
          </div>
          </div>
      </body>
    </html></code>
